import requests
from bs4 import BeautifulSoup
from unidecode import unidecode
import pandas as pd
import os
import time
import random

data = []
for entry in os.listdir('example_input_directory'):
    excel_file_df = pd.read_excel('example_input_directory\\' + entry)
    for i in range(len(excel_file_df)):
        URL = excel_file_df.iloc[i, 1]
        id = URL.split('/')[4][4:]
        new_URL = 'https://www.digikala.com/ajax/product/questions/{}/?page=1'.format(id)
        print('start read...')
        try:
            r = requests.get(new_URL)
            print('end read...')
            soup = BeautifulSoup(r.content, 'html5lib')
            table = soup.find('div', attrs={'class': 'c-question__list'})
            page_counter = 2
            if table != None:
                while True:
                    for row in table.findAll('div', attrs={'class': 'c-question__item js-question-container'}):
                        like_number = 0
                        counter = 0
                        main_answer = ''
                        for answer in row.findAll('div', attrs={'class': 'c-question__reply js-answer'}):
                            counter += 1
                            likes = answer.find('div', attrs={
                                'class': 'o-btn c-question__feed-back-btn c-question__feed-back-btn--positive js-answer-like'})
                            likes = int(unidecode(likes.contents[0]))
                            if likes >= like_number:
                                like_number = likes
                                main_answer = answer.find('div', attrs={'class': 'c-question__reply-body'})
                                main_answer = main_answer.contents[0].replace('\n', ' ').lstrip().rstrip()

                        if counter > 0:
                            main_question = row.find('div', attrs={'class': 'c-question__item-title'})
                            main_question = main_question.contents[0].replace('\n', ' ').lstrip().rstrip()
                            data.append([main_question, main_answer])

                    new_URL = new_URL[:-1] + str(page_counter)
                    page_counter += 1
                    r = requests.get(new_URL)
                    soup = BeautifulSoup(r.content, 'html5lib')
                    table = soup.find('div', attrs={'class': 'c-question__list'})
                    if table == None:
                        break

            print(i)
        except Exception as e:
            time.sleep(random.randint(300,400))
            continue
df = pd.DataFrame(data, columns=['Question', 'Answer'])
df.to_csv('question_answer.csv')
print("++++++++++++end+++++++++++")

